﻿insert into clusters_log (cluster, node, manufacturer, operation, date)
select c."cluster", c.node, c.manufacturer, 'insert', CURRENT_DATE
from clusters as c left outer join clusters_log as cl 
on c.cluster = cl.cluster and c.node = cl.node where cl.node is null; --firstly added clusters

update clusters_log set date = CURRENT_DATE, operation = 'insert'
from (select cl.id as clid from clusters as c join clusters_log as cl
on c.cluster = cl.cluster and c.node = cl.node) as f
where f.clid = clusters_log.id; --secondly added clusters

update clusters_log set date = CURRENT_DATE, operation = 'delete'
from (select cl.id as clid from clusters as c right outer join clusters_log as cl
on c.cluster = cl.cluster and c.node = cl.node where c.node is null)as f
where f.clid = clusters_log.id; --deleted clusters

--truncate table clusters_log
--delete from clusters where cluster = 1 and node = 4 --for debug
--insert into clusters(cluster, node, manufacturer) values (1,3,1); --for debug
select * from clusters_log;
