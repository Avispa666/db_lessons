﻿--create table "user" (
--id serial primary key,
--username text not null,
--"password" bytea not null)
--alter table "user" alter created_time type timestamptz
--alter table "user" alter "password" type text
create or replace function t_user_handler() returns trigger as $t_user$
begin
	if TG_OP = 'INSERT' then
		NEW.created_time := now();
		NEW."password" := md5(NEW."password");
	end if;
	if TG_OP = 'UPDATE' and OLD."password" != md5(NEW."password") then
		OLD."password" := md5(NEW."password");
	end if;
	return NEW;
end;
$t_user$ language plpgsql;
create trigger "t_user" before insert or update on "user"
for each row execute procedure t_user_handler()

insert into "user"(username, "password") values ('login1', 'password1')
select * from "user"
update "user" set password = 'login1' where id = 3 returning *