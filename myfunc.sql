﻿CREATE or replace FUNCTION myfunc() RETURNS integer AS $$
declare myline record;
begin
for myline in select * from animals loop
	insert into animals_count ("type", "color", "count") 
	values (myline.type, myline.color, 
	(select count(id) from animals where "type" = myline.type and "color" = myline.color));
	delete from animals where "type" = myline.type and "color" = myline.color;
end loop;
delete from animals_count where "count" = 0;
return 1;
END;
$$ LANGUAGE plpgsql;