﻿create table product (
id serial,
product text);
create table product_history (
id serial,
product text,
create_date timestamptz)
alter table product_history add column "action" text

create or replace function t_user_handler() returns trigger as $t_product$
begin
	if TG_OP = 'INSERT' then
		NEW.created_time := now();
		NEW."password" := md5(NEW."password");
	end if;
	if TG_OP = 'UPDATE' and OLD."password" != md5(NEW."password") then
		OLD."password" := md5(NEW."password");
	end if;
	return NEW;
end;
$t_product$ language plpgsql;
create trigger "t_product" after delete or update on ""
for each row execute procedure t_user_handler()
