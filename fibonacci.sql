﻿create temp table if not exists fibonacci(
num bigint unique not null,
val bigint unique not null);

create or replace function fib(i bigint) returns integer as $$
declare
	f bigint;
begin
	if i = 0 then
		return 0;
	end if;
	if i = 1 then
		return 1;
	end if;
	select val into f from fibonacci where num = i;
	if f is not null then
		return f;
	end if;
	f := fib(i-1) + fib(i-2);
	insert into fibonacci values (i, f);
	return f;
end;
$$ language plpgsql;

select * from fib(10); -- nth fibonacci number
select * from fibonacci;