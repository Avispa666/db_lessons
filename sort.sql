﻿create or replace function sort(arr integer[]) returns integer[] as $$
declare 
	i integer;
	j integer;
	t integer;
begin
	for i in 1..array_length(arr, 1) loop
		for j in 1..(array_length(arr, 1) - i) loop
			if arr[j] > arr[j+1] then
				t := arr[j];
				arr[j] := arr[j+1];
				arr[j+1] := t;
			end if;
		end loop;
	end loop;
	return arr;
end;
$$ language plpgsql;

select * from sort(array[2,3,1,5,-1,8,4,-6]);