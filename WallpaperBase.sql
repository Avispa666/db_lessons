CREATE DATABASE "WallpapersBase"
	WITH OWNER = postgres
	ENCODING = 'UTF8'
	TABLESPACE = pg_default
	LC_COLLATE = 'English_United States.1252'
	LC_CTYPE = 'English_United States.1252'
	CONNECTION LIMIT = -1;
	   
CREATE EXTENSION plpgsql
	SCHEMA pg_catalog
	VERSION "1.0";

------------------Trigger functions------------------------------------------

CREATE OR REPLACE FUNCTION public.t_unmoderated_logger() --logging of unmoderated table
  RETURNS trigger AS
$BODY$
declare
	_time timestamptz;
begin
	_time := now();
	if TG_OP = 'INSERT' then
		insert into "unmoderated_log" (user_id,
		category,tags,filename,operation,log_time) 
		values (NEW.user_id, NEW.category, NEW.tags, NEW.filename, TG_OP, _time);
	end if;
	if TG_OP = 'DELETE' then
		insert into "unmoderated_log" (user_id,
		category,tags,filename,operation,log_time) 
		values (OLD.user_id, OLD.category, OLD.tags, OLD.filename, TG_OP, _time);
	end if;
	if TG_OP = 'UPDATE' then
		insert into "unmoderated_log" (user_id,
		category,tags,filename,operation,log_time) 
		values (OLD.user_id, OLD.category, OLD.tags, OLD.filename, 'UPDATE(DEL)', _time);
		insert into "unmoderated_log" (user_id,
		category,tags,filename,operation,log_time) 
		values (NEW.user_id, NEW.category, NEW.tags, NEW.filename, 'UPDATE(INS)', _time);
	end if;
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.t_unmoderated_logger() OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.t_users_logger() RETURNS trigger AS --logging of users table
$BODY$
declare
	_time timestamptz;
begin
	_time := now();
	if TG_OP = 'INSERT' then
		insert into "users_log" (username, operation, log_time) values (NEW.username, TG_OP, _time);
	end if;
	if TG_OP = 'DELETE' then
		insert into "users_log" (username, operation, log_time) values (OLD.username, TG_OP, _time);
	end if;
	if TG_OP = 'UPDATE' then
		insert into "users_log" (username, operation, log_time) values (OLD.username, 'UPDATE(DEL)', _time);
		insert into "users_log" (username, operation, log_time) values (NEW.username, 'UPDATE(INS)', _time);
	end if;
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.t_users_logger() OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.t_admins_logger() RETURNS trigger AS --logging of admins table
$BODY$
declare
	_time timestamptz;
begin
	_time := now();
	if TG_OP = 'INSERT' then
		insert into "admins_log" (user_id, operation, log_time) values (NEW.id, TG_OP, _time);
	end if;
	if TG_OP = 'DELETE' then
		insert into "admins_log" (user_id, operation, log_time) values (OLD.id, TG_OP, _time);
	end if;
	if TG_OP = 'UPDATE' then
		insert into "admins_log" (user_id, operation, log_time) values (OLD.id, 'UPDATE(DEL)', _time);
		insert into "admins_log" (user_id, operation, log_time) values (NEW.id, 'UPDATE(INS)', _time);
	end if;
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.t_admins_logger() OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.t_votes_logger() RETURNS trigger AS --logging of votes table
$BODY$
declare
	_time timestamptz;
begin
	_time := now();
	if TG_OP = 'INSERT' then
		insert into "votes_log" (wallpaper_id, user_id, vote, operation, log_time) values (NEW.wallpaper_id, NEW.user_id, NEW.vote, TG_OP, _time);
	end if;
	if TG_OP = 'DELETE' then
		insert into "votes_log" (wallpaper_id, user_id, vote, operation, log_time) values (OLD.wallpaper_id, OLD.user_id, OLD.vote, TG_OP, _time);
	end if;
	if TG_OP = 'UPDATE' then
		insert into "votes_log" (wallpaper_id, user_id, vote, operation, log_time) values (OLD.wallpaper_id, OLD.user_id, OLD.vote, 'UPDATE(DEL)', _time);
		insert into "votes_log" (wallpaper_id, user_id, vote, operation, log_time) values (NEW.wallpaper_id, NEW.user_id, NEW.vote, 'UPDATE(INS)', _time);
	end if;
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.t_votes_logger() OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.t_comment_handler() RETURNS trigger AS -- sets comment creation time to now
$BODY$
begin

	NEW.create_time := now();
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.t_comment_handler() OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.t_user_img_handler() RETURNS trigger AS --sets default profile image for newly registered users
$BODY$
begin
	if NEW.profile_img is null then
		NEW.profile_img = 'default.png';
	end if;
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.t_user_img_handler() OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.t_wallpaper_set_name() RETURNS trigger AS --sets wallpaper filename as id.ext if it's contatins only extension
$BODY$
begin
	if substring(NEW.picture,1,1) = '.' then
		NEW.picture := concat(NEW.id, NEW.picture); 
	end if;
	return NEW;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.t_wallpaper_set_name() OWNER TO postgres;

--------------------------Functions--------------------------------------

CREATE OR REPLACE FUNCTION public.fill_users_uploads()
  RETURNS integer AS
$BODY$
begin
	drop table if exists users_uploads;
	create temp table users_uploads as select wallpaper_id, username
	from uploads join users on uploads.user_id = users.id order by wallpaper_id asc;
	return 0;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.fill_users_uploads() OWNER TO postgres;


---------------------Tables-----------------------------------------

CREATE TABLE public.unmoderated_logs --logging unmoderated wallpapers
(
  id serial,
  user_id integer,
  category text,
  tags text,
  filename text,
  operation text,
  log_time timestamp with time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.unmoderated_logs OWNER TO postgres;

CREATE TABLE public.votes_log --logging votes
(
  id serial,
  wallpaper_id integer,
  user_id integer,
  vote vote,
  operation text,
  log_time timestamp with time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.votes_log OWNER TO postgres;
  
CREATE TABLE public.admins
(
  id integer NOT NULL,
  CONSTRAINT admin_fk0 FOREIGN KEY (id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.admins OWNER TO postgres;

CREATE TABLE public.admins_log --logging table of admins
(
  id serial,
  user_id integer,
  "operation" text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.admins_log OWNER TO postgres;

CREATE TABLE public.users_log --logging users table
(
  id serial,
  username varchar(20),
  operation text,
  log_time timestamp with time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.users_log
  OWNER TO postgres;
  
CREATE TABLE public.categories --wallpapers' categories table
(
  id serial,
  name text NOT NULL,
  CONSTRAINT categories_pkey PRIMARY KEY (id),
  CONSTRAINT categories_name_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.categories OWNER TO postgres;

CREATE TABLE public.comments --comments table
(
  id serial,
  wallpaper_id integer NOT NULL,
  user_id integer NOT NULL,
  "text" text NOT NULL,
  create_time timestamp with time zone,
  CONSTRAINT comment_pk PRIMARY KEY (id),
  CONSTRAINT comment_fk0 FOREIGN KEY (wallpaper_id)
      REFERENCES public.wallpapers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT comment_fk1 FOREIGN KEY (user_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.comments OWNER TO postgres;
  
CREATE TABLE public.downloads --wallpapers downloads by user
(
  id serial,
  user_id integer NOT NULL,
  wallpaper_id integer NOT NULL,
  CONSTRAINT downloads_pkey PRIMARY KEY (id),
  CONSTRAINT download_fk0 FOREIGN KEY (user_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT download_fk1 FOREIGN KEY (wallpaper_id)
      REFERENCES public.wallpapers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.downloads OWNER TO postgres;

CREATE TABLE public.tag_names --names of wallpapers' tags
(
  id serial,
  name text NOT NULL,
  CONSTRAINT tag_names_pkey PRIMARY KEY (id),
  CONSTRAINT unique_name UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tag_names OWNER TO postgres;

CREATE TABLE public.tags --tags of wallpapers' (many to many)
(
  id serial,
  tag_id integer NOT NULL,
  wallpaper_id integer NOT NULL,
  CONSTRAINT theme_pk PRIMARY KEY (id),
  CONSTRAINT tags_fk0 FOREIGN KEY (tag_id)
      REFERENCES public.tag_names (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tags_wallpaper_id_fkey FOREIGN KEY (wallpaper_id)
      REFERENCES public.wallpapers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tags OWNER TO postgres;

CREATE TABLE public.tokens --using for securely remember me feature
(
  uuid text NOT NULL,
  token bytea NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT tokens_pkey PRIMARY KEY (user_id),
  CONSTRAINT tokens_fk0 FOREIGN KEY (user_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "user_id unique" UNIQUE (user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tokens OWNER TO postgres;

CREATE TABLE public.unmoderated --unmoderated wallpapers
(
  id serial,
  user_id integer NOT NULL,
  category text NOT NULL,
  tags text NOT NULL,
  filename text NOT NULL,
  CONSTRAINT unmoderated_pkey PRIMARY KEY (id),
  CONSTRAINT unmoderated_fk0 FOREIGN KEY (user_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.unmoderated OWNER TO postgres;

CREATE TABLE public.uploads --wallpapers uploaded by users
(
  id serial,
  user_id integer NOT NULL,
  wallpaper_id integer NOT NULL,
  CONSTRAINT uploads_pkey PRIMARY KEY (id),
  CONSTRAINT load_fk0 FOREIGN KEY (user_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT load_fk1 FOREIGN KEY (wallpaper_id)
      REFERENCES public.wallpapers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.uploads OWNER TO postgres;

CREATE TABLE public.users --table of users
(
  id serial,
  username character varying(20) NOT NULL,
  password bytea NOT NULL,
  profile_img text,
  salt bytea NOT NULL,
  CONSTRAINT user_pk PRIMARY KEY (id),
  CONSTRAINT user_login_key UNIQUE (username)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.users OWNER TO postgres;
  
CREATE TABLE public.votes --wallpaper votes
(
  id serial,
  wallpaper_id integer NOT NULL,
  user_id integer,--can be null if voter hasn't logged in
  vote vote NOT NULL,
  CONSTRAINT votes_fk0 FOREIGN KEY (wallpaper_id)
      REFERENCES public.wallpapers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT votes_fk1 FOREIGN KEY (user_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.votes OWNER TO postgres;

CREATE TABLE public.wallpapers --wallpapers table
(
  id serial,
  picture text NOT NULL,
  category_id integer,
  CONSTRAINT wallpaper_pk PRIMARY KEY (id),
  CONSTRAINT wallpapers_fk0 FOREIGN KEY (category_id)
      REFERENCES public.categories (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.wallpapers OWNER TO postgres;

----------------------------------Triggers---------------------------------------------

CREATE TRIGGER t_unmoderated_log
  AFTER INSERT OR UPDATE OR DELETE
  ON public.unmoderated
  FOR EACH ROW
  EXECUTE PROCEDURE public.t_unmoderated_logger();
  
CREATE TRIGGER t_votes_log
  AFTER INSERT OR UPDATE OR DELETE
  ON public.votes
  FOR EACH ROW
  EXECUTE PROCEDURE public.t_votes_logger();
  
CREATE TRIGGER t_admins_log 
  AFTER UPDATE OR INSERT OR DELETE
  ON "admins"
  FOR EACH ROW 
  EXECUTE PROCEDURE t_admins_logger();

CREATE TRIGGER t_comment_date
  BEFORE INSERT
  ON public.comments
  FOR EACH ROW
  EXECUTE PROCEDURE public.t_comment_handler();

CREATE TRIGGER t_name
  AFTER INSERT
  ON public.wallpapers
  FOR EACH ROW
  EXECUTE PROCEDURE public.t_wallpaper_set_name();

CREATE TRIGGER t_user_img
  AFTER UPDATE OR DELETE
  ON public.users
  FOR EACH ROW
  EXECUTE PROCEDURE public.t_user_img_handler();

CREATE TRIGGER t_users_log
  AFTER INSERT OR UPDATE OR DELETE
  ON public.users
  FOR EACH ROW
  EXECUTE PROCEDURE public.t_users_logger();
  
-----------------------Views--------------------------------------

CREATE OR REPLACE VIEW public.average_moderation_interval AS --average moderation interval by all wallpapers
 SELECT avg(f.times) AS average
   FROM ( SELECT max(unmoderated_logs.log_time) - min(unmoderated_logs.log_time) AS times
           FROM unmoderated_logs
          WHERE unmoderated_logs.operation = 'INSERT'::text OR unmoderated_logs.operation = 'DELETE'::text
          GROUP BY unmoderated_logs.filename) f;

ALTER TABLE public.average_moderation_interval
  OWNER TO postgres;

CREATE OR REPLACE VIEW public.registration_stats AS --count of registrations by day of week
 SELECT to_char(max(users_log.log_time), 'day'::text) AS day,
    count(users_log.username) AS count
   FROM users_log
  WHERE users_log.operation = 'INSERT'::text
  GROUP BY (date_part('isodow'::text, users_log.log_time));

ALTER TABLE public.registration_stats OWNER TO postgres;

CREATE OR REPLACE VIEW public.upload_stats AS --top 10 uploaders among users
 SELECT max(users.username::text) AS username,
    count(uploads.wallpaper_id) AS count
   FROM uploads
     JOIN users ON uploads.user_id = users.id
  GROUP BY uploads.user_id
  ORDER BY (count(uploads.wallpaper_id)) DESC
 LIMIT 10;

ALTER TABLE public.upload_stats OWNER TO postgres;

------------------------Indexes-------------------------------

CREATE INDEX comments_by_wallpaper_id -- get comments where wallpaper_id = ?
  ON public.comments
  USING btree
  (wallpaper_id);

CREATE INDEX wallpapers_by_id --get wallpapers where id > ? limit 20
  ON public.wallpapers
  USING btree
  (id);
  
CREATE INDEX categories_by_id --get categories where id > ? limit 20
  ON public.categories
  USING btree
  (id);
  
CREATE INDEX tags_by_wallpaper_id --get tags for wallpaper
  ON public.tags
  USING btree
  (wallpaper_id);
  
CREATE INDEX categories_by_name
  ON public.categories
  USING btree
  (name COLLATE pg_catalog."default");
  
CREATE INDEX tag_names_by_name
  ON public.tag_names
  USING btree
  (name COLLATE pg_catalog."default");
  
CREATE INDEX votes_by_wallpaper_id
  ON public.votes
  USING btree
  (wallpaper_id);
  
CREATE INDEX downloads_by_user_id
  ON public.downloads
  USING btree
  (user_id);

CREATE INDEX uploads_by_user_id
  ON public.uploads
  USING btree
  (user_id);