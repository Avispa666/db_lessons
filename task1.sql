﻿create table animal(
"id" serial not null primary key,
"type" text not null,
"color" text not null
);

insert into animal("type", color) values('dog', 'black');
insert into animal("type", color) values('cat', 'gray');
insert into animal("type", color) values('dog', 'white');
insert into animal("type", color) values('fox', 'red');
insert into animal("type", color) values('dog', 'black');
create table animal_counter as select "type", color, count(*) from animal group by "type", color;

ALTER TABLE animal_counter ADD COLUMN id SERIAL PRIMARY KEY;
select * from animal_counter;